(ns minesweeper.board)

;; This abstraction barrier defines the encapsulation of the
;; board-related procedures: constructors, accessors and
;; mutators (non-destructive, since all these are pure functions).

;; The board is represented by each cell having a REAL state and a
;; GAME state. REAL states are: EMPTY, BOMB or NUMBER [1-8]. GAME
;; states are: HIDDEN, FLAGGED or one of the real states: EMPTY or
;; NUMBER.

;; The board data structure is a map with its indexes as keys and
;; cells as values. Cells are also maps in the form of {:game STATE
;; :real STATE]. Besides indexes, the board contains an item whose key
;; is :rows-cols and whose value is {:rows-count VAL :cols-count VAL}.

;; In some procedures operating with indexes is easier than doing it
;; with the row-col pair, and vice versa. 
(defn row-and-col-from-index
  "Return the corresponding [ROW COL] of a given INDEX."
  [cols-count index]
  (let [col (mod index cols-count)
        row (/ (- index col) cols-count)]
    [row col]))

(defn index-from-row-and-col
  "Return the corresponding INDEX of a given [ROW COL]."
  [cols-count [row col]]
  (+ (* cols-count row) col))

;; Used for placing the bombs randomly in the board.
(defn rand-list
  "Return a vector of COUNT random natural numbers from 0 to MAX, not included."
  [count max]
  (if (> count max)
    (throw (list "Error: RAND-LIST: COUNT > MAX" count max))
    (loop [count count
           num-list (range max)
           rand-list '()]
      (if (zero? count)
        rand-list
        (let [rand-num (rand-nth num-list)]
          (recur (dec count)
                 (remove #(= rand-num %) num-list)
                 (conj rand-list rand-num)))))))

;; Used for placing the numbers after placing the bombs.      
(defn adjacent-cells
  "Return a list of the adjacent cells (indexes) that are not outside of the board."
  [rows-count cols-count index]
  ;; This procedure is better understood working with row and col
  ;; pairs instead of indexes.
  (let [[row col] (row-and-col-from-index cols-count index)
        rows (vector (dec row) row (inc row))
        cols (vector (dec col) col (inc col))
        ;; Unfiltered [ROW COL] list.
        row-col-list
        (mapcat (fn [row]
                  (map (fn [col]
                         [row col])
                       cols))
                rows)
        ;; Filter out those that are outside of the board and the cell
        ;; itself.
        row-col-list-filtered
        (filter (fn [[current-row current-col]]
                  (and (not= [row col] [current-row current-col])
                       (>= current-row 0) (<= current-row (dec rows-count))
                       (>= current-col 0) (<= current-col (dec cols-count))))
                row-col-list)]
    ;; Turn the [row col] pairs back into indexes.
    (map (fn [[row col]]
           (index-from-row-and-col cols-count [row col]))
         row-col-list-filtered)))
(adjacent-cells 3 3 2)

(defn filter-out-seqs
  "Filter out all the elements of SEQ-FILTER from SEQ."
  [seq-filter seq]
  (filter (fn [elm]
            (not (some #(= elm %) seq-filter)))
          seq))

(defn create-board
  "Create a game-ready board: with BOMBS, EMPTY and NUMBERS in its real state and all HIDDEN in its game state."
  [rows-count cols-count bombs-count]
  (let [size (* rows-count cols-count)]
    (if (or (>= bombs-count size) (< bombs-count 1))
      (throw (list "Error: BOARD: Bad BOMBS-COUNT SIZE"
                   bombs-count
                   size))
      ;; List of the index of each bomb.
      (let [bombs-indexes (rand-list bombs-count size)
            ;; List of the indexes of each bomb's adjacent cells. Some
            ;; indexes will be repeated. Filter out the indexes that
            ;; correspond to bombs.
            bombs-adjacent
            (filter-out-seqs bombs-indexes
                             (mapcat #(adjacent-cells
                                       rows-count cols-count %)
                                     bombs-indexes))
            ;; Non-repeated list of adjacents.
            bombs-adjacent-distinct
            (distinct bombs-adjacent)
            ;; Map of INDEX NUMBER of each of those adjacent cells.
            numbers-indexes
            (loop [adjacent-list bombs-adjacent
                   result-map {}]
              (if (empty? adjacent-list)
                result-map
                (let [rest-of-adjacents (rest adjacent-list)
                      index (first adjacent-list)
                      current-num (get result-map index)]
                  (if (nil? current-num)
                    ;; If the index is new, add it with the number 1.
                    (recur rest-of-adjacents
                           (assoc result-map index 1))
                    ;; If the index is already there, increment the
                    ;; number.
                    (recur rest-of-adjacents
                           (assoc result-map index
                                  (inc current-num)))))))
            ;; Find the list of indexes that are not bombs nor
            ;; numbers. Those will be EMPTY.
            empty-indexes
            (filter-out-seqs
             (concat bombs-indexes bombs-adjacent-distinct)
             (range size))]
        ;; Create the board with the three lists we have. Build lists
        ;; of [INDEX VALUE] vectors, concat the lists and transform
        ;; this resulting list into a map which already contains the
        ;; rows-cols item.
        (into {:rows-cols {:rows-count rows-count
                           :cols-count cols-count}}
              (concat
               ;; List of bombs.
               (map #(vector % {:game :hidden :real :bomb})
                    bombs-indexes)
               ;; List of empties.
               (if (empty? empty-indexes)
                 '()
                 (map #(vector % {:game :hidden :real :empty})
                      empty-indexes))
               ;; List of numbers.
               (map #(vector % {:game :hidden :real (get numbers-indexes %)})
                    bombs-adjacent-distinct)))))))

;; Rudimentary board display. Useful for testing purposes.
(defn display-board
  "Display one of the two states of the board as a string."
   [state board]
  (if (and (not= state :game) (not= state :real))
      (throw (list "Error: DISPLAY-BOARD: incorrect STATE"
                   state))
      (let [rows-count (get-in board [:rows-cols :rows-count])
            cols-count (get-in board [:rows-cols :cols-count])
            size (* rows-count cols-count)
            ;; Symbols to display, depending on the cell's state value.
            symbols-map (into {:hidden 'O :flagged 'V :empty 'H :bomb 'B}
                              ;; The [1-8] possible numbers.
                              (map #(vector % %) (range 1 9)))]
        ;; Get a list of all the item's symbols.
        (map #(get symbols-map (get-in board [% state]))
             (range size)))))

;; This is needed since the built-in CONTAINS? doesn't work for
;; transient sequences. This needs to be implemented in terms of NTH
;; and COUNT.
(defn transient-contains?
  "The CONTAINS? procedure implemented for transient sequences."
  [seq elem]
  (loop [i (dec (count seq))]
    (cond (< i 0) false
          (= elem (nth seq i)) true
          :else (recur (dec i)))))

;; This is a text-book example of how sometimes assignment is
;; needed. In this recursive process, when you visit a node, before
;; further descending, you need to know if this node has already been
;; visited in other branch. This is easily done by keeping a mutable
;; data structure.
(defn adjacent-empties-and-numbers
  "Return a list with the indexes of the adjacent empty and number cells of an empty cell, recurring for the empty cells."
  [index board]
  (let [rows-count (get-in board [:rows-cols :rows-count])
        cols-count (get-in board [:rows-cols :cols-count])]
    ;; The index has to be inserted since it will be adjacent for
    ;; other cells.
    (def empty-indexes (transient [index]))
    (def number-indexes (transient []))
    (defn iter [adjacent-indexes]
      ;; Capture all the adjacent :empty and number cells that are not
      ;; already in their respective vectors.
      (let [new-empties
            (filter #(and (not (transient-contains? empty-indexes %))
                          (= :empty (get-in board [% :real])))
                    adjacent-indexes)
            new-numbers
            (filter #(and (not (transient-contains? number-indexes %))
                          (number? (get-in board [% :real])))
                    adjacent-indexes)]
        ;; Add the new empties and numbers into their respective vectors.
        ;; Also recur for the new empties adjacents.
        (doseq [i new-numbers]
          (conj! number-indexes i))
        (doseq [i new-empties]
          (conj! empty-indexes i)
          (iter (adjacent-cells rows-count cols-count i)))))
    (iter (adjacent-cells rows-count cols-count index))
    ;; Return a list of the indexes of both vectors. All of them will
    ;; be revealed.
    (concat (persistent! empty-indexes)
            (persistent! number-indexes))))

(defn toggle-flag
  "Flag or unflag a cell of the board."
  ([index board]
   (case (get-in board [index :game])
     :hidden (assoc-in board [index :game] :flagged)
     :flagged (assoc-in board [index :game] :hidden)
     ;; Else, don't change the board
     board))
  ;; If the row and cols are supplied instead, convert them to index.
  ([row col board]
   (toggle-flag (index-from-row-and-col
                 (get-cols-count board)
                 [row col])
                board)))

(defn reveal-cell
  "Reveal the real state of a cell."
  [index board]
  (assoc-in board [index :game]
            (get-in board [index :real])))

;; Will be used for numbers and empties.
(defn reveal-cells
  "Reveal the state of multiple cells."
  [indexes board]
  (loop [indexes indexes
         board board]
    (let [index (first indexes)]
      (if (nil? index)
        board
        (recur (rest indexes)
               (reveal-cell index board))))))

;; For finding out the winning condition.
(defn count-cells-type
  "Count all the cells of a TYPE in a STATE. For numbers, use
  TYPE :number."
  [type state board]
  ;; Predicate to test the cell type.
  (def cell-type-pred
    (if (= type :number) number? #(= % type)))
  (count (filter (fn [[key val]]
                   ;; Only for cells, not the :rows-cols key.
                   (and (number? key) (cell-type-pred (get val state))))
                 board)))

;; This will be checked every time an empty or number cell is
;; revealed. Check if all the non-bomb cells have been revealed.
(defn game-won?
  "Return TRUE if the board state allows to infer that the game has
  been won, return FALSE otherwise"
  [board]
  (= (+ (count-cells-type :number :game board)
        (count-cells-type :empty :game board))
     (+ (count-cells-type :number :real board)
        (count-cells-type :empty :real board))))

;; Remember how the game works. When you reveal a bomb, you lose. When
;; you reveal a number, nothing else happens. And when you reveal an
;; empty cell, all the empty or number adjacent cells get revealed
;; too (recursively for the empty cells).
(defn commit-cell
  "Commit a cell of the board. Returns the board and the game state:
  {:state :PLAYING/:WON/:LOST :board BOARD}"
  ([index board]
   (let [real-state (get-in board [index :real])]
     (cond
       ;; If it is a bomb, lose directly.
       (= :bomb real-state) {:state :lost :board board}
       ;; If it's an empty, reveal all the necessary cells.
       (= :empty real-state)
       (let [board (reveal-cells (adjacent-empties-and-numbers index board)
                                 board)]
         {:state (if (game-won? board) :won :playing)
          :board board})
       ;; If it's a number, reveal it.
       :else
       (let [board (reveal-cell index board)]
         {:state (if (game-won? board) :won :playing)
          :board board}))))
  ([row col board]
   (commit-cell (index-from-row-and-col
                  (get-cols-count board) [row col])
                board)))

;; Three more procedures to be exported.
(defn get-cell [row col state board]
 "Get the cell's state from a board."
 (let [cols-count (get-cols-count board)
       index (index-from-row-and-col cols-count [row col])]
   (get-in board [index state])))

(defn get-rows-count [board]
  (get-in board [:rows-cols :rows-count]))
(defn get-cols-count [board]
  (get-in board [:rows-cols :cols-count]))
