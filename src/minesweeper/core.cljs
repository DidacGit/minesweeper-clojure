(ns minesweeper.core
  (:use [minesweeper.board
         :only (create-board toggle-flag commit-cell
                             get-rows-count get-cols-count get-cell)]))

(enable-console-print!)

;; This variable contains all the data representing the game. It's the
;; only piece of state that there will be. It contains the game board,
;; the game difficulty type and the game state.

;; Three game types are defined, depending on the number of rows,
;; columns and bombs (:EASY :NORMAL and :HARD). And three game states
;; are also defined: :PLAYING :WON :LOST.
(defn create-game
  "Initialize the game."
  [type]
  {:board (case type
            :easy (create-board 6 6 4)
            :normal (create-board 9 9 12)
            :hard (create-board 12 12 24)
            (throw (list "Error: CREATE-GAME: Incorrect game type"
                         type)))
   :type type
   :state :playing})

;; By default, the game is normal.
(defonce *game (atom (create-game :normal)))

(defn start-game
  "Start a new game with the selected type."
  [type]
  (reset! *game (create-game type)))

(defn restart-game
  "Restart a game of the same type."
  []
  (start-game (get @*game :type)))

;; This is used for interacting with the board. It can also change the
;; game state.
(defn change-game
  "Change the board and/or the state of the game."
  ([board]
   (reset! *game {:board board
                  :type (get @*game :type)
                  :state (get @*game :state)}))
  ([board state]
   (reset! *game {:board board
                  :type (get @*game :type)
                  :state state})))

;; Display parameters:
(def canvas-size 700)
(def cell-size 35)
;; Size of the bombs and flags.
(def circ-size (* cell-size 0.6))
;; Position of the options, size and spacing between them.
(def options-x 50)
(def options-y 20)
(def options-height 40)
(def options-width 120)
(def options-spacing 10)
(def options-text-size (/ options-width 5))
;; Text that gives you a hint.
(def hint-text-size 15)
(def hint-text-x options-x)
(def hint-text-y (+ options-y options-height (* 3 hint-text-size)))
;; Position of the board.
(def initial-x options-x)
(def initial-y (+ hint-text-y hint-text-size))
;; Size of the WON and LOST messages.
(def messages-size 35)

(defn draw-hidden-cell [x y]
  (js/strokeWeight 2)
  (js/stroke 51)
  (js/fill 224 224 224)
  (js/rect x y cell-size cell-size))  

(defn draw-flagged-cell [x y]
  (draw-hidden-cell x y)
  (js/noStroke)
  (js/fill 255 0 0)
  (js/ellipse (+ x (/ cell-size 2))
              (+ y (/ cell-size 2))
              circ-size circ-size))

(defn draw-empty-cell [x y]
  (js/strokeWeight 2)
  (js/stroke 51)
  (js/fill 138 138 138)
  (js/rect x y cell-size cell-size))  

(defn draw-bomb-cell [x y]
  (draw-empty-cell x y)
  (js/noStroke)
  (js/fill 0 0 0)
  (js/ellipse (+ x (/ cell-size 2))
              (+ y (/ cell-size 2))
              circ-size circ-size))

(defn draw-number-cell [x y num]
  (draw-empty-cell x y)
  (js/fill 0 0 0)
  (js/textAlign js/CENTER js/CENTER)
  (js/textSize (* cell-size 0.6))
  (js/text num (+ x (/ cell-size 2))
               (+ y (/ cell-size 2))))
  
(defn draw-cell [x y type]
  (case type
    :hidden (draw-hidden-cell x y)
    :flagged (draw-flagged-cell x y)
    :empty (draw-empty-cell x y)
    :bomb (draw-bomb-cell x y)
    ;; Number cells:
    (draw-number-cell x y type)))

;; Used in DRAW-BOARD.
(defn get-draw-point
  "Get the point [X Y] where a cell must be drawn."
  [row col]
  [(+ initial-x (* cell-size col))
   (+ initial-y (* cell-size row))])
  
(defn draw-board
  "Draw the board according to STATE."
  [board state]
  (let [rows-count (get-rows-count board)
        cols-count (get-cols-count board)]
    (doseq [col (range cols-count)
            row (range rows-count)]
      (let [[x y] (get-draw-point row col)
            type (get-cell row col state board)]
        (draw-cell x y type)))))

(defn draw-over-canvas
  "Re-display the canvas."
  []
  (js/noStroke)
  (js/fill 255 255 255)
  (js/rect 0 0 canvas-size canvas-size))

;; Used to display the win or lose message.
(defn draw-message
  "Draw a message in the center of the board."
  [message]
  (let [{board :board} @*game
        rows (get-rows-count board)
        cols (get-cols-count board)
        x (+ initial-x (* cell-size (/ cols 2)))
        y (+ initial-y (* cell-size (/ rows 2)))]
    (js/textAlign js/CENTER)
    (js/textSize messages-size)
    (js/stroke 0 0 0)
    (js/text message x y)))

(defn draw-won-message []
  (js/fill 0 255 0)
  (draw-message "YOU WON!"))

(defn draw-lost-message []
  (js/fill 255 0 0)
  (draw-message "YOU LOST"))

(defn draw-hint-text
  "Draw the text that gives you a hint."
  []
  (js/textSize hint-text-size)
  (js/textAlign js/LEFT js/BOTTOM)
  (js/noStroke)
  (js/fill 0 0 0)
  (js/text "(Hold Shift to flag)" hint-text-x hint-text-y))

(defn draw-options
  "Draw the difficulty options with the selected one highlighted."
  []
  (let [type (get @*game :type)
        easy-x options-x
        normal-x (+ options-x options-width options-spacing)
        hard-x (+ options-x (* options-width 2) (* options-spacing
                                                   2))]
    ;; Draw the corresponding box.
    (js/noStroke)
    (js/fill 222 222 222)
    (js/rect (case type
               :easy easy-x
               :normal normal-x
               :hard hard-x
               (throw (list "Error: DRAW-OPTIONS: incorrect TYPE"
                            type)))
             options-y options-width options-height)
    ;; Draw the text.
    (js/textAlign js/CENTER js/CENTER)
    (js/textSize options-text-size)
    (js/fill 0 0 0)
    (js/text "EASY" (+ easy-x (/ options-width 2))
             (+ options-y (/ options-height 2)))
    (js/text "NORMAL" (+ normal-x (/ options-width 2))
             (+ options-y (/ options-height 2)))
    (js/text "HARD" (+ hard-x (/ options-width 2))
             (+ options-y (/ options-height 2)))))

;; Used to find out the cell that the player pressed.
(defn get-cell-from-coord
  "Return the cell's position: [ROW COL] that's in a given
  coordinate. It may return cells outside the board."
  [x y]
  [(Math/floor (/ (- y initial-y) cell-size))
   (Math/floor (/ (- x initial-x) cell-size))])

;; Helper function for TOUCH-STARTED.
(defn check-options-positions
  "Returns true if the X is positioned inside the option number NUM."
  [x num]
  (and (> x (+ options-x
               (* options-width num)
               (* options-spacing num)))
       (< x (+ options-x
               (* options-width (+ num 1))
               (* options-spacing num)))))

;; Function called once at the beginning of the program.
(defn setup []
  (js/createCanvas canvas-size canvas-size))

;; Function called whenever there's a mouse click.
(defn touch-started []
  (println)
  (let [x js/mouseX y js/mouseY
        {board :board state :state} @*game
        rows-count (get-rows-count board)
        cols-count (get-cols-count board)]
    ;; Either a cell or a difficulty option could have been pressed.
    (cond (and (> x initial-x)
               (< x (+ initial-x (* cols-count cell-size)))
               (> y initial-y)
               (< y (+ initial-y (* rows-count cell-size))))
          ;; If it's a cell:
          (let [[row col] (get-cell-from-coord x y)]
            (cond
              ;; If the game has ended, restart it.
              (not= state :playing) (restart-game)
              ;; If shift is pressed, flag the cell.
              (js/keyIsDown 16)
              (change-game (toggle-flag row col board))
              ;; Else, commit the cell.
              :else (let [{state :state
                           board :board}
                          (commit-cell row col board)]
                      (change-game board state))))
          ;; Check for the three options.
          (and (> y options-y) (< y (+ options-y options-height)))
          (cond (check-options-positions x 0) (start-game :easy)
                (check-options-positions x 1) (start-game :normal)
                (check-options-positions x 2) (start-game :hard)))))

;; Function called every time the screen gets drawn.
(defn draw []
  ;; Re-cover the entire canvas each time.
  (draw-over-canvas)
  ;; Display the options menu and the hint.
  (draw-options)
  (draw-hint-text)
  ;; Display the board.
  (let [{board :board type :type state :state} @*game]
    (case state
      :playing (draw-board board :game)
      :won (do (draw-board board :real)
               (draw-won-message))
      :lost (do (draw-board board :real)
                (draw-lost-message))
      (throw (list "Error: DRAW: wrong STATE" state)))))

(doto js/window
  (aset "setup" setup)
  (aset "draw" draw)
  (aset "touchStarted" touch-started))
  
